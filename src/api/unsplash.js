import axios from 'axios';

const UNSPLASH_KEY =
  '405339c9a57a84a2661604142123f97b62132a8be57d6eba97344a86fbc615e7';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: `Client-ID ${UNSPLASH_KEY}`,
  },
});
