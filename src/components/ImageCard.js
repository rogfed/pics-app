import React, { useState, createRef, useEffect } from 'react';
import PropTypes from 'prop-types';

const ImageCard = ({ src, alt }) => {
  const [spans, setSpans] = useState(0);

  const imageRef = createRef(null);

  useEffect(() => {
    imageRef.current.addEventListener('load', calculateSpans);
  }, []);

  const calculateSpans = () => {
    const height = imageRef.current.clientHeight;
    const spans = Math.ceil(height / 10);
    setSpans(spans);
  };

  return (
    <div style={{ gridRowEnd: `span ${spans}` }}>
      <img
        alt={alt ? alt : 'No description available at this time.'}
        src={src}
        ref={imageRef}
      />
    </div>
  );
};

ImageCard.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
};

export default ImageCard;
