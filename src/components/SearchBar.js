import React, { useState } from 'react';
import PropTypes from 'prop-types';

const SearchBar = ({ onSubmit }) => {
  const [term, setTerm] = useState('');

  const onFormSubmit = e => {
    e.preventDefault();
    onSubmit(term);
  };

  const onInputChange = e => setTerm(e.target.value);

  return (
    <div className='ui segment'>
      <form className='ui form' onSubmit={onFormSubmit}>
        <div className='field'>
          <label>Image Search</label>
          <input type='text' onChange={onInputChange} value={term} />
        </div>
      </form>
    </div>
  );
};

SearchBar.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default SearchBar;
