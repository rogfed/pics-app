import React from 'react';
import PropTypes from 'prop-types';

import ImageCard from './ImageCard';

import '../styles/ImageList.scss';

const ImageList = ({ images }) => {
  const imagesList = images.map(({ urls, id, alt_description }) => (
    <ImageCard src={urls.regular} key={id} alt={alt_description} />
  ));

  return <div className='image-list'>{imagesList}</div>;
};

ImageList.propTypes = {
  images: PropTypes.array.isRequired,
};

export default ImageList;
